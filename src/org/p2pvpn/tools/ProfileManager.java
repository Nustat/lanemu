/*
    Copyright 2008, 2009 Wolfgang Ginolas
    Copyright 2024 Nikolay Borodin <Monsterovich>

    This file is part of Lanemu.

    Lanemu is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Lanemu is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Lanemu.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.p2pvpn.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProfileManager {

    private final static Properties props = new Properties();

    private final String file;
    private final File profileFile;

    private static String networkName = "default";

    private final static String KV_FORMAT = "%s.%s";
    
    public ProfileManager() {
        file = "profile.ini";
        profileFile = new File(file);
    }
    
    public ProfileManager(String file) {
        this.file = file;
        profileFile = new File(file);
    }

    public ProfileManager load(String network) {
        networkName = network;

        try {
            read(profileFile);
        } catch (IOException ex) {
            Logger.getLogger(ProfileManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return this;
    }

    public void flush() {
        try {
            write(profileFile);
        } catch (IOException ex) {
            Logger.getLogger(ProfileManager.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void putGlobal(String key, String val) {
        props.setProperty(key, val);
    }

    public void put(String key, String val) {
        props.setProperty(getPropertyKey(key), val);
    }

    public void putInt(String key, Integer val) {
        props.setProperty(getPropertyKey(key), val.toString());
    }

    public void putBoolean(String key, Boolean val) {
        props.setProperty(getPropertyKey(key), val.toString());
    }

    public void putDouble(String key, Double val) {
        props.setProperty(getPropertyKey(key), val.toString());
    }

    public String getGlobal(String key, String def) {
        return props.getProperty(key, def);
    }

    public String get(String key, String def) {
        return props.getProperty(getPropertyKey(key), def);
    }

    public Integer getInt(String key, Integer def) {
        return Integer.valueOf(props.getProperty(getPropertyKey(key), def.toString()));
    }

    public Boolean getBoolean(String key, Boolean def) {
        return props.getProperty(getPropertyKey(key), def.toString()).equals("true");
    }

    public Double getDouble(String key, Integer def) {
        Double d = 0.0;
        try {
            d = Double.valueOf(props.getProperty(getPropertyKey(key), def.toString()));
        } catch (NumberFormatException e) {
        }
        return d;
    }

    private String getPropertyKey(String keyName) {
        return String.format(KV_FORMAT, networkName, keyName);
    }

    public void remove(String key) {
        props.remove(key);
    }

    public ProfileDescription[] getProfiles() {
        final List<ProfileDescription> result = new ArrayList<ProfileDescription>();

        for (Entry<Object, Object> entry : props.entrySet()) {
            final String key = entry.getKey().toString();
            final String value = entry.getValue().toString();

            if (key.endsWith(".name")) {
                result.add(new ProfileDescription(
                    key.substring(0, key.length() - 5),
                    value));
            }
        }

        ProfileDescription[] resultArray = new ProfileDescription[result.size()];
        result.toArray(resultArray);

        Arrays.sort(resultArray, new Comparator<ProfileDescription>() {
            @Override
            public int compare(ProfileDescription p1, ProfileDescription p2) {
                return p1.getName().compareTo(p2.getName());
            }
        });

        return resultArray;
    }

    public static void setDefaults(String defaultName) {
        final String[][] defaults = {
            {"name", "no name"},
            {"serverPort", "0"},
            {"upnp", "0"},
            {"macBlacklist", ""},
            {"peerBlacklist", ""},
            {"sendLimit", "0.0"},
            {"recLimit", "0.0"},
            {"sendBufferSize", "100"},
            {"tcpFlush", "0"}
        };
        
        for (String[] property : defaults) {
            props.setProperty(String.format(KV_FORMAT, defaultName, property[0]), property[1]);
        }
    }

    private static void read(File file) throws IOException {
        try (FileInputStream fis = new FileInputStream(file)) {
            props.load(fis);
        }
    }

    private static void write(File file) throws IOException {
        try (FileOutputStream fos = new FileOutputStream(file)) {
            props.store(fos, "");
        }
    }

    public class ProfileDescription {

        private final String key;
        private final String name;

        public ProfileDescription(String key, String name) {
            this.key = key;
            this.name = name;
        }

        public String getKey() {
            return key;
        }

        public String getName() {
            return name;
        }
    }
}
