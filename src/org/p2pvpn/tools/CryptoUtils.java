/*
    Copyright 2009 Wolfgang Ginolas
    Copyright 2023-2025 Nikolay Borodin <Monsterovich>

    This file is part of Lanemu.

    Lanemu is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Lanemu is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Lanemu.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.p2pvpn.tools;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.Signature;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

/**
 * Utility class to create different cryptographic algorithms.
 *
 * @author wolfgang
 */
public class CryptoUtils {

    private static final int RSA_KEYSIZE = 2048;
    
    public static final int GCM_NONCE_LENGTH = 12; // in bytes
    private static final int GCM_TAG_LENGTH = 16; // in bytes

    static {
        initBC();
    }

    /**
     * Initialize the bouncy castle provider.
     */
    static private void initBC() {
        Security.addProvider(new BouncyCastleProvider());
    }

    /**
     * @return a SecureRandom object
     */
    static public SecureRandom getSecureRandom() {
        try {
            return SecureRandom.getInstance("SHA1PRNG");
        } catch (NoSuchAlgorithmException t) {
            Logger.getLogger("").log(Level.SEVERE, null, t);
            assert false;
            return null;
        }
    }

    /**
     * @return a Signature object
     */
    static public Signature getSignature() {
        try {
            return Signature.getInstance("SHA512withRSA", "BC");
        } catch (NoSuchAlgorithmException | NoSuchProviderException t) {
            Logger.getLogger("").log(Level.SEVERE, null, t);
            assert false;
            return null;
        }
    }

    /**
     * @return a new KeyPair usable to sign things
     */
    static public KeyPair createSignatureKeyPair() {
        try {
            KeyPairGenerator g = KeyPairGenerator.getInstance("RSA", "BC");
            g.initialize(RSA_KEYSIZE, getSecureRandom());
            return g.generateKeyPair();
        } catch (NoSuchAlgorithmException | NoSuchProviderException t) {
            Logger.getLogger("").log(Level.SEVERE, null, t);
            assert false;
            return null;
        }
    }

    /**
     * @return a asymmetric Cipher
     */
    static public Cipher getAsymmetricCipher() {
        try {
            return Cipher.getInstance("RSA/NONE/PKCS1Padding", "BC");
        } catch (NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException t) {
            Logger.getLogger("").log(Level.SEVERE, null, t);
            assert false;
            return null;
        }
    }

    /**
     * @return a KeyPair that cna be used or an asymmetric cipher
     */
    static public KeyPair createEncryptionKeyPair() {
        return createSignatureKeyPair();		// also uses RSA
    }

    /**
     * Convert a byte array to a RSA public key
     *
     * @param ekey the byte array
     * @return the public key
     */
    static public PublicKey decodeRSAPublicKey(byte[] ekey) {
        try {
            X509EncodedKeySpec spec = new X509EncodedKeySpec(ekey);
            KeyFactory factory = KeyFactory.getInstance("RSA", "BC");
            return (RSAPublicKey) factory.generatePublic(spec);
        } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeySpecException t) {
            Logger.getLogger("").log(Level.SEVERE, null, t);
            assert false;
            return null;
        }
    }

    /**
     * Convert a byte array to a RSA private key
     *
     * @param ekey the byte array
     * @return the private key
     */
    static public PrivateKey decodeRSAPrivateKey(byte[] ekey) {
        try {
            PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(ekey);
            KeyFactory factory = KeyFactory.getInstance("RSA", "BC");
            return (RSAPrivateKey) factory.generatePrivate(spec);
        } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeySpecException t) {
            Logger.getLogger("").log(Level.SEVERE, null, t);
            assert false;
            return null;
        }
    }

    /**
     * @return a hash algorithm
     */
    static public MessageDigest getMessageDigest() {
        try {
            return MessageDigest.getInstance("SHA1");
        } catch (NoSuchAlgorithmException t) {
            Logger.getLogger("").log(Level.SEVERE, null, t);
            assert false;
            return null;
        }
    }

    /**
     * @return a symmetric cipher
     */
    static public Cipher getSymmetricCipher() {
        try {
            return Cipher.getInstance("AES/GCM/NoPadding", "BC");
        } catch (NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException t) {
            Logger.getLogger("").log(Level.SEVERE, null, t);
            assert false;
            return null;
        }
    }

    /**
     * @return the key length of the symmetric cipher in bytes
     */
    static public int getSymmetricKeyLength() {
        return 16;
    }

    /**
     * Create a symmetric key from bytes.
     *
     * @param b the bytes
     * @return the symmetric key
     */
    static public SecretKey decodeSymmetricKey(byte[] b) {
        return new SecretKeySpec(b, 0, getSymmetricKeyLength(), "AES");
    }
    
    /**
     * This method constructs a GCMParameterSpec.
     * The first GCM_NONCE_LENGTH bytes of the buffer is assumed to be the nonce.
     *
     * @param nonce nonce byte array
     * @return GCM parameter spec
     */
    static public AlgorithmParameterSpec getAlgorithmParamSpec(byte[] nonce) {
        return new GCMParameterSpec(GCM_TAG_LENGTH * 8, nonce, 0, GCM_NONCE_LENGTH);
    }
    
    /**
     * This method creates a random nonce of length defined by
     * GCM_NONCE_LENGTH.
     *
     * @return nonce byte array
     */
    static public byte[] getNonce() {
        byte[] nonce = new byte[GCM_NONCE_LENGTH];
        SecureRandom rnd = CryptoUtils.getSecureRandom();
        rnd.nextBytes(nonce);
        return nonce;
    }
}
