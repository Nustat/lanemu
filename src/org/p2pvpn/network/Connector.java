/*
    Copyright 2008, 2009 Wolfgang Ginolas
    Copyright 2023-2025 Nikolay Borodin <Monsterovich>

    This file is part of Lanemu.

    Lanemu is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Lanemu is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Lanemu.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.p2pvpn.network;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.p2pvpn.tools.AdvProperties;
import org.p2pvpn.tools.SocketAddrStr;

/**
 * This class maintains a list of known hosts and tries to connect them
 * periodically.
 *
 * @author Wolfgang Ginolas
 */
public class Connector {

    private final static long RETRY_S = 5 * 60;
    private final static long REMOVE_MS = 60 * 60 * 1000;

    ConnectionManager connectionManager;
    final Map<Endpoint, EndpointInfo> ips;

    final Vector<ConnectorListener> listeners;

    /**
     * Create a new Connector
     *
     * @param connectionManager the ConnectionManager
     */
    public Connector(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
        listeners = new Vector<>();
        ips = new HashMap<>();
    }

    /**
     * Add a ConnectorListener.
     *
     * @param l the listener
     */
    public void addListener(ConnectorListener l) {
        synchronized (listeners) {
            listeners.add(l);
        }
    }

    /**
     * Remove a Connector Listener
     *
     * @param l the listener
     */
    public void removeListener(ConnectorListener l) {
        synchronized (listeners) {
            listeners.remove(l);
        }
    }

    /**
     * Notify all ConnectorListeners.
     */
    private void notifyListeners() {
        ConnectorListener[] ls;
        synchronized (listeners) {
            ls = listeners.toArray(new ConnectorListener[0]);
        }
        for (ConnectorListener l : ls) {
            try {
                l.ipListChanged(this);
            } catch (Throwable t) {
                Logger.getLogger("").log(Level.WARNING, "", t);
            }
        }
    }

    /**
     * Return all known hosts.
     *
     * @return the hosts
     */
    public Endpoint[] getIPs() {
        synchronized (ips) {
            return ips.keySet().toArray(new Endpoint[0]);
        }
    }

    /**
     * Return information about the given IP.
     *
     * @param e the IP
     * @return the information
     */
    public EndpointInfo getIpInfo(Endpoint e) {
        synchronized (ips) {
            return ips.get(e);
        }
    }
    
    /**
     * Remove obsolete IP addresses that failed to connect to. 
     * New addresses that are added in the current session remain intact.
     *
     * @param ip the IP
     * @param port the port
     * @param source the source of peer addition, could be "stored", "bootstrap", "BitTorrent tracker" or "BitTorrent DHT"
     */
    public void removeObsoleteIP(InetAddress ip, int port, String source) {
        try {
            synchronized (ips) {
                for (Endpoint endPoint : ips.keySet()) {
                    if (endPoint.getInetAddress().equals(ip) && endPoint.getPort() == port) {
                        if (source == null || ips.get(endPoint).source.equals(source)) {
                            ips.remove(endPoint);
                        }
                        break;
                    }
                }
            }
        } catch (UnknownHostException e) {
        }
        notifyListeners();
    }

    /**
     * Add an IP to the known hosts.
     *
     * @param ip the IP
     * @param port the port
     * @param peerID the PeerID ot null when it's unknown
     * @param source the source of this information
     * @param keep keep the ip in the list forever?
     */
    public void addIP(byte[] ip, int port, PeerID peerID, String source, boolean keep) {
        Endpoint endpoint = new Endpoint(ip, port);
        EndpointInfo endpointInfo = new EndpointInfo(peerID, source, keep);
        if (!connectionManager.getScheduledExecutor().isShutdown()) {
            connectionManager.getScheduledExecutor().schedule(
                new AddIPLater(endpoint, endpointInfo), 0, TimeUnit.SECONDS);
        }
    }

    /**
     * Add an IP to the known hosts.
     *
     * @param ip the IP
     * @param port the port
     * @param peerID the PeerID ot null when it's unknown
     * @param source the source of this information
     * @param keep keep the ip in the list forever?
     */
    public void addIP(String ip, int port, PeerID peerID, String source, boolean keep) {
        try {
            addIP(InetAddress.getByName(ip).getAddress(), port, peerID, source, keep);
        } catch (UnknownHostException ex) {
            Logger.getLogger("").log(Level.WARNING,
                String.format("An obscure ip address %s was obtained from peer %s.",
                    ip, peerID.toString())
            );
        }
    }

    /**
     * Add an IP to the known hosts.
     *
     * @param ip the IP
     * @param port the port
     * @param peerID the PeerID ot null when it's unknown
     * @param source the source of this information
     * @param keep keep the ip in the list forever?
     */
    public void addIP(InetAddress ip, int port, PeerID peerID, String source, boolean keep) {
        addIP(ip.getAddress(), port, peerID, source, keep);
    }

    /**
     * Add the IPs given in the access invitation.
     *
     * @param p the access invitation
     */
    public void addIPs(AdvProperties p) {
        int i = 0;

        while (p.containsKey("network.bootstrap.connectTo." + i)) {
            try {
                InetSocketAddress a = SocketAddrStr.parseSocketAddr(p.getProperty("network.bootstrap.connectTo." + i));
                addIP(a.getAddress(), a.getPort(), null, "bootstrap", true);
            } catch (Exception t) {
                Logger.getLogger("").log(Level.WARNING, "", t);
            }
            i++;
        }
    }

    /**
     * Schedule a connection attempt.
     *
     * @param e the host to connect
     * @param delay the delay
     */
    private void scheduleConnect(Endpoint e, long delay) {
        connectionManager.getScheduledExecutor().schedule(new ConnectRunnable(e), delay, TimeUnit.SECONDS);
    }

    /**
     * A Thread which adds an IP to the known host list.
     */
    private class AddIPLater implements Runnable {

        private final Endpoint endpoint;
        private final EndpointInfo endpointInfo;

        /**
         * Add the given IP later.
         *
         * @param endpoint the Host
         * @param endpointInfo host information
         */
        public AddIPLater(Endpoint endpoint, EndpointInfo endpointInfo) {
            this.endpoint = endpoint;
            this.endpointInfo = endpointInfo;
        }

        /**
         * Add the IP.
         */
        @Override
        public void run() {
            boolean schedule = false;
            synchronized (ips) {
                if (!ips.containsKey(endpoint)) {
                    ips.put(endpoint, endpointInfo);
                    schedule = true;
                } else {
                    ips.get(endpoint).update(endpointInfo.peerID,
                        endpointInfo.getSource(), endpointInfo.isKeepForEver());
                }
            }
            if (schedule) {
                scheduleConnect(endpoint, 1);
            }
            notifyListeners();
        }
    }

    /**
     * Try to connect to the given Host.
     */
    private class ConnectRunnable implements Runnable {

        Endpoint e;

        /**
         * Try to connect to the given Host.
         *
         * @param e the host
         */
        public ConnectRunnable(Endpoint e) {
            this.e = e;
        }

        /**
         * Connect the host.
         */
        @Override
        public void run() {
            try {
                EndpointInfo info;
                synchronized (ips) {
                    info = ips.get(e);
                }
                if (info == null) {
                    return;
                }
                
                boolean hasConnection = connectionManager.getRouter().isConnectedTo(info.peerID) || 
                    connectionManager.getRouter().isConnectedTo(e.getInetAddress(), e.getPort());

                if (!hasConnection) {
                    try {
                        connectionManager.connectTo(e.getInetAddress(), e.getPort());
                    } catch (UnknownHostException ex) {
                    }
                } else {
                    info.update();
                }

                if (System.currentTimeMillis() - REMOVE_MS > info.timeAdded) {
                    synchronized (ips) {
                        ips.remove(e);
                    }

                    notifyListeners();
                }
            } catch (UnknownHostException t) {
            }
            scheduleConnect(e, RETRY_S);
        }

    }

    /**
     * *
     * A Host (IP and port).
     */
    public class Endpoint {

        byte[] ip;
        int port;

        /**
         * Create a new Endpoint.
         *
         * @param ip the ip
         * @param port the port
         */
        public Endpoint(byte[] ip, int port) {
            this.ip = ip;
            this.port = port;
        }

        public byte[] getIp() {
            return ip;
        }

        public InetAddress getInetAddress() throws UnknownHostException {
            return InetAddress.getByAddress(ip);
        }

        public int getPort() {
            return port;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Endpoint other = (Endpoint) obj;
            if (this.port != other.port) {
                return false;
            }
            return Arrays.equals(ip, other.ip);
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 59 * hash + (this.ip != null ? Arrays.hashCode(ip) : 0);
            hash = 59 * hash + this.port;
            return hash;
        }

        @Override
        public String toString() {
            try {
                return SocketAddrStr.socketAddrToStr(new InetSocketAddress(InetAddress.getByAddress(ip), port));
            } catch (UnknownHostException ex) {
                return "unknown host";
            }
        }
    }

    /**
     * Information about a host.
     */
    public final class EndpointInfo {

        PeerID peerID;
        long timeAdded;
        String source;
        boolean keepForEver;

        /**
         * Create a new EndpointInfo.
         *
         * @param peerID the PeerID
         * @param source the source of this host
         * @param status the status of the host
         * @param keepForEver keep this host for ever?
         */
        EndpointInfo(PeerID peerID, String source, boolean keepForEver) {
            this.peerID = peerID;
            this.source = source;
            this.keepForEver = keepForEver;
            update();
        }

        void update() {
            timeAdded = System.currentTimeMillis();
        }

        /**
         * Update the hist info
         *
         * @param peerID the PeerID
         * @param source the source of this host
         * @param keepForEver keep this host for ever?
         */
        void update(PeerID peerID, String source, boolean keepForEver) {
            if (peerID != null) {
                this.peerID = peerID;
            }
            if (source != null) {
                this.source = source;
            }
            if (keepForEver) {
                this.keepForEver = true;
            }
            timeAdded = System.currentTimeMillis();
        }

        public boolean isKeepForEver() {
            return keepForEver;
        }

        public String getSource() {
            return source;
        }

        public PeerID getPeerID() {
            return peerID;
        }

        public long getTimeAdded() {
            return timeAdded;
        }
    }
}
