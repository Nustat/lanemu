/*
    Copyright 2024 Nikolay Borodin <Monsterovich>

    This file is part of Lanemu.

    Lanemu is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Lanemu is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Lanemu.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.p2pvpn.network.bittorrent;

import java.security.MessageDigest;
import org.p2pvpn.tools.CryptoUtils;

/**
 * Common BitTorrent functions
 * @author monsterovich
 */
public class BitTorrent {
    /**
     * Calculate a hash for the network, using the signature of the network.
     *
     * @param signature network signature
     * @param maxLen length of the hash
     * @return the hash
     */
    public static byte[] networkHash(byte[] signature, int maxLen) {
        MessageDigest md = CryptoUtils.getMessageDigest();
        md.update("BitTorrent".getBytes());	// make sure the key differs from
        // other hashes created from the publicKey
        byte[] hash = md.digest(signature);
        byte[] result = new byte[Math.min(maxLen, hash.length)];
        System.arraycopy(hash, 0, result, 0, result.length);
        return result;
    }
}
