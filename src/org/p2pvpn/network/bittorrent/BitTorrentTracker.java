/*
    Copyright 2008, 2009 Wolfgang Ginolas
    Copyright 2023-2025 Nikolay Borodin <Monsterovich>

    This file is part of Lanemu.

    Lanemu is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Lanemu is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Lanemu.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.p2pvpn.network.bittorrent;

import org.p2pvpn.network.bittorrent.bencode.BencodeString;
import org.p2pvpn.network.bittorrent.bencode.Bencode;
import org.p2pvpn.network.*;
import java.io.IOException;
import java.io.PushbackInputStream;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.net.URLCodec;
import org.p2pvpn.network.bittorrent.bencode.BencodeInt;

/**
 * This class connects to a BitTorrent tracker, to find other peers.
 * (http://www.bittorrent.org/beps/bep_0003.html)
 *
 * @author Wolfgang Ginolas
 */
public class BitTorrentTracker implements Runnable {

    private static final int REFRESH_S = 10 * 60;

    private final ConnectionManager connectionManager;
    private final String tracker;
    private final byte[] peerId;

    /**
     * Create a new BitTorrentTracker object which periodically polls an tracker
     * to find other peers.
     *
     * @param connectionManager the ConnectionManager
     * @param tracker the trakcer url
     */
    public BitTorrentTracker(ConnectionManager connectionManager, String tracker) {
        this.connectionManager = connectionManager;
        this.tracker = tracker;
        peerId = new byte[Contact.CONTACT_ID_LEN];
        new Random().nextBytes(peerId);
        schedule(1);
    }

    /**
     * Schedule a tracker poll
     *
     * @param seconds poll in <code>secongs</codee> seconds
     */
    private void schedule(int seconds) {
        connectionManager.getScheduledExecutor().schedule(this, seconds, TimeUnit.SECONDS);
    }

    /**
     * Poll the tracker.
     *
     * @param hash the hash of the cuttent network
     * @param port the local port
     * @return a Bencode-Map
     * @throws java.net.MalformedURLException
     * @throws java.io.IOException
     */
    private Map<Object, Object> trackerRequest(byte[] hash, int port) throws MalformedURLException, IOException {
        String sUrl = tracker + "?info_hash=" + new String(new URLCodec().encode(hash))
            + "&port=" + port + "&compact=1&peer_id=" + new String(new URLCodec().encode(peerId))
            + "&uploaded=0&downloaded=0&left=100";

        URL url = new URL(sUrl);
        try (PushbackInputStream in = new PushbackInputStream(url.openStream())) {
            return (Map<Object, Object>) Bencode.parseBencode(in);
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * Poll the tracker and add the returned IPs to the known hosts list.
     */
    @Override
    public void run() {
        int nextRequest = REFRESH_S;
        try {
            Map<Object, Object> res = trackerRequest(
                BitTorrent.networkHash(connectionManager.getAccessCfg().getPropertyBytes("network.signature", null), Contact.CONTACT_ID_LEN), 
                connectionManager.getServerPort()
            );

            BencodeInt minInterval = (BencodeInt) res.get(new BencodeString("min interval"));
            if (minInterval != null) {
                nextRequest = minInterval.getInt();
            }

            BencodeString peersString = (BencodeString) res.get(new BencodeString("peers"));
            
            if (peersString != null) {
                byte[] peers = peersString.getBytes();

                for (int i = 0; i < peers.length; i += 6) {
                    byte[] ipb = new byte[4];
                    System.arraycopy(peers, i, ipb, 0, 4);
                    InetAddress ip = Inet4Address.getByAddress(ipb);
                    int portLow = 0xFF & (int) peers[i + 5];
                    int portHi = 0xFF & (int) peers[i + 4];
                    int port = (portHi << 8) + portLow;

                    addIP(ip, port);
                }
            }
            
            BencodeString peers6String = (BencodeString) res.get(new BencodeString("peers6"));
            
            if (peers6String != null) {
                byte[] peers = peers6String.getBytes();
                
                for (int i = 0; i < peers.length; i += 18) {
                    byte[] ipb = new byte[16];
                    System.arraycopy(peers, i, ipb, 0, 16);
                    InetAddress ip = Inet6Address.getByAddress(ipb);

                    int portLow = 0xFF & (int) peers[i + 17];
                    int portHi = 0xFF & (int) peers[i + 16];
                    int port = (portHi << 8) + portLow;

                    addIP(ip, port);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger("").log(Level.SEVERE, String.format("Unable to request tracker \"%s\"", tracker), ex);
        }

        schedule(nextRequest);
    }
    
    /**
     * Add the ip to the list if it has not already been added.
     *
     * @param hash the peer ip
     * @param port the peer port
     * @throws java.net.UnknownHostException
     */
    void addIP(InetAddress ip, int port) throws UnknownHostException {
        boolean ipAdded = false;
        for (Connector.Endpoint endPoint : connectionManager.getConnector().getIPs()) {
            if (endPoint.getInetAddress().equals(ip) && endPoint.getPort() == port) {
                ipAdded = true;
                break;
            }
        }

        if (!ipAdded) {
            connectionManager.getConnector().addIP(ip.getAddress(), port, null, "BitTorrent tracker", false);
        }
    }
}
