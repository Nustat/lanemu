/*
    Copyright 2008, 2009 Wolfgang Ginolas
    Copyright 2023-2025 Nikolay Borodin <Monsterovich>

    This file is part of Lanemu.

    Lanemu is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Lanemu is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Lanemu.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.p2pvpn.network;

import java.util.StringTokenizer;

/**
 * This class represents an Mac-Address.
 *
 * @author wolfgang
 */
public class MacAddress {

    private long address;
    
    private static final int MAC_ADDR_LENGTH = 6;
    private static final long MULTICAST_MASK = new MacAddress("1:0:0:0:0:0").address;

    /**
     * Create a new MacAddress using 6 bytes in the array starting at the given
     * offset
     *
     * @param address the byte array
     * @param off the offset
     */
    public MacAddress(byte[] address, int off) {
        setAddress(address, off);
    }

    /**
     * Create a new MacAddress
     *
     * @param address the address (6 bytes)
     */
    public MacAddress(byte[] address) {
        setAddress(address);
    }

    /**
     * Create a new MacAddress
     *
     * @param str the address using the format "xx:xx:xx:xx:xx:xx"
     */
    public MacAddress(String str) {
        byte[] addr = new byte[MAC_ADDR_LENGTH];
        StringTokenizer st = new StringTokenizer(str, ":");
        int i;

        i = 0;
        while (i < MAC_ADDR_LENGTH && st.hasMoreTokens()) {
            addr[i] = (byte) Integer.parseInt(st.nextToken(), 16);
            i++;
        }

        setAddress(addr);
    }

    /**
     * Set the address.
     *
     * @param address the byte array
     * @param off the offset
     */
    public void setAddress(byte[] address, int off) {
        assert address.length >= off + MAC_ADDR_LENGTH;

        this.address = 0;
        for (int i = 0; i < MAC_ADDR_LENGTH; i++) {
            this.address <<= 8;
            this.address += ((int) address[i + off]) & 0xFF;
        }
    }

    /**
     * Set the address.
     *
     * @param address the address
     */
    public void setAddress(byte[] address) {
        setAddress(address, 0);
    }

    /**
     * Return the address as byte array
     *
     * @return the address
     */
    public byte[] getAddress() {
        byte[] result = new byte[MAC_ADDR_LENGTH];

        long addr = address;

        for (int i = MAC_ADDR_LENGTH - 1; i >= 0; i--) {
            result[i] = (byte) (addr & 0xFF);
            addr >>= 8;
        }
        return result;
    }

    /**
     * In the first octet of MAC address, bit 0 has been reserved for broadcast or multicast traffic. 
     * When we have unicast traffic, this bit will be set to 0. 
     * For broadcast or multicast traffic, this bit will be set to 1.
     *
     * @return is this mac address a broadcast address?
     */
    public boolean isBroadcast() {
        return 0 != (address & MULTICAST_MASK);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (address ^ (address >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        MacAddress other = (MacAddress) obj;
        return address == other.address;
    }

    @Override
    public synchronized String toString() {
        byte[] a = getAddress();
        StringBuilder result = new StringBuilder("");

        for (int i = 0; i < MAC_ADDR_LENGTH; i++) {
            if (i != 0) {
                result.append(':');
            }
            int b = ((int) a[i]) & 0xFF;
            if (b < 0x10) {
                result.append('0');
            }
            result.append(Integer.toString(b, 16));
        }

        return result.toString();
    }

}
