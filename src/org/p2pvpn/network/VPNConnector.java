/*
    Copyright 2008, 2009 Wolfgang Ginolas
    Copyright 2023-2025 Nikolay Borodin <Monsterovich>

    This file is part of Lanemu.

    Lanemu is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Lanemu is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Lanemu.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.p2pvpn.network;

import java.util.Arrays;
import org.p2pvpn.tuntap.TunTap;

/**
 * This claas establishes a connection between the virtual network adapter and
 * the Router.
 *
 * @author wolfgang
 */
public class VPNConnector implements Runnable {

    private final static byte IPV4_HIGH = 0x08;
    private final static byte IPV4_LOW = 0x00;
    private final static byte IPV4_UDP = 17;

    private static final byte MIN_PACKET_LENGTH = 14 + 20;
    private static final byte IPV4_HIGH_INDEX = 12;
    private static final byte IPV4_LOW_INDEX = 13;
    private static final byte IPV4_UDP_INDEX = 14 + 9;
    private static final byte IPV4_HEADER_LENGTH = 20;
    private static final byte IPV4_SOURCE_IP_INDEX = 26;
    private static final byte IPV4_CHECKSUM_INDEX = 14 + 10;
    private static final byte UDP_CHECKSUM_INDEX = 14 + 20 + 6;
    
    private static final int MAX_BUFFER_SIZE = 2048;
    private static final byte MIN_PACKET_SIZE = 12;

    private final TunTap tuntap;
    private Router router;
    private final Thread thread;

    private static VPNConnector vpnConnector = null;

    /**
     * Create a new VPNConnector.
     *
     * @throws java.lang.Exception
     */
    private VPNConnector() throws Exception {
        tuntap = TunTap.createTunTap();
        router = null;

// fixme: Oracle JVM crashes when executing this code (OpenJDK works fine)
//        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
//            public void run() {
//                if (tuntap != null) {
//                    tuntap.close();
//                }
//            }
//        }));

        thread = new Thread(this, "VPNConnector");
        thread.start();
    }

    /**
     * Get the global VPNConnector.
     *
     * @return
     * @throws java.lang.Exception
     */
    public static VPNConnector getVPNConnector() throws Exception {
        if (vpnConnector == null) {
            vpnConnector = new VPNConnector();
        }
        return vpnConnector;
    }

    /**
     * Set the Router.
     *
     * @param router the Router.
     */
    public void setRouter(Router router) {
        this.router = router;
        if (router != null) {
            router.setVpnConnector(this);
        }
    }

    public TunTap getTunTap() {
        return tuntap;
    }

    /**
     * Send an packet to the virtual network adapter.
     *
     * @param packet the packet
     */
    public void receive(byte[] packet) {
        tuntap.write(packet, packet.length);
    }

    /**
     * Force the correct local IP in an UDP broadcast packet. This is necessary
     * becaus Windows sometimes uses the wrong source IP in broadcast packets.
     *
     * @param packet the packet
     */
    private void forceIP(byte[] packet) {
        if (packet.length >= MIN_PACKET_LENGTH) {
            // Check if the packet is IPv4 and UDP
            if (packet[IPV4_HIGH_INDEX] == IPV4_HIGH && packet[IPV4_LOW_INDEX] == IPV4_LOW && packet[IPV4_UDP_INDEX] == IPV4_UDP) {
                byte[] ip = tuntap.getIPBytes();   // Get the local IP address
                // Check if the source IP in the packet is different from the local IP
                if (packet[IPV4_SOURCE_IP_INDEX] != ip[0] || packet[IPV4_SOURCE_IP_INDEX + 1] != ip[1]
                    || packet[IPV4_SOURCE_IP_INDEX + 2] != ip[2] || packet[IPV4_SOURCE_IP_INDEX + 3] != ip[3]) {

                    int checksum = 0;
                    packet[IPV4_CHECKSUM_INDEX] = 0;  // Set the IPv4 header checksum to 0 for recalculation
                    packet[IPV4_CHECKSUM_INDEX + 1] = 0;
                    System.arraycopy(ip, 0, packet, IPV4_SOURCE_IP_INDEX, 4);   // Replace the source IP in the packet

                    // Recalculate the IPv4 header checksum
                    for (int i = IPV4_HIGH_INDEX; i < IPV4_HIGH_INDEX + IPV4_HEADER_LENGTH; i += 2) {
                        checksum += ((0xFF & packet[i]) << 8) + (0xFF & packet[i + 1]);
                    }

                    // Handle carry bits in the checksum
                    while ((checksum & 0xFFFF0000) != 0) {
                        checksum = (checksum & 0xFFFF) + (checksum >> 16);
                    }

                    checksum = ~checksum;

                    // Set the new IPv4 header checksum
                    packet[IPV4_CHECKSUM_INDEX] = (byte) (0xFF & (checksum >> 8));
                    packet[IPV4_CHECKSUM_INDEX + 1] = (byte) (0xFF & checksum);

                    packet[UDP_CHECKSUM_INDEX] = 0;   // Unset the UDP checksum
                    packet[UDP_CHECKSUM_INDEX + 1] = 0;
                }
            }
        }
    }

    /**
     * A thread that reads packets from the virtual network adapter and sends
     * them to the Router.
     */
    @Override
    public void run() {
        byte[] buffer = new byte[MAX_BUFFER_SIZE];

        while (true) {
            int bytesRead = tuntap.read(buffer);

            if (bytesRead >= MIN_PACKET_SIZE) {
                byte[] packet = Arrays.copyOf(buffer, bytesRead);
                forceIP(packet);

                if (router != null) {
                    router.send(packet);
                }
            }
        }
    }
}
