/*
    Copyright 2008, 2009 Wolfgang Ginolas
    Copyright 2023-2025 Nikolay Borodin <Monsterovich>

    This file is part of Lanemu.

    Lanemu is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Lanemu is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Lanemu.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.p2pvpn.tuntap;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The TunTap class for Windows
 *
 * @author Wolfgang Ginolas
 */
public class TunTapWindows extends TunTap {

    static {
        String libname = null;
        try {
            switch (System.getProperty("os.arch")) {
                case "x86":
                case "i386":
                case "i486":
                case "i586":
                case "i686":
                    libname = "clib\\libTunTapWindows.dll";
                    break;
                case "x86_64":
                case "amd64":
                    libname = "clib\\libTunTapWindows64.dll";
                    break;
                default:
                    Logger.getLogger("").log(Level.WARNING, "Unknown os.arch. Still trying to load libTunTapWindowsUnknown.dll");
                    libname = "clib\\libTunTapWindowsUnknown.dll";
                    break;
            }
            loadLib(libname);
        } catch (Throwable e) {
            Logger.getLogger("").log(Level.SEVERE, "Could not load tap driver library", e);
        }
    }

    private long cPtr;
    private String dev;

    /**
     * Create a new TunTapWindows.
     *
     * @throws java.lang.Exception
     */
    public TunTapWindows() throws Exception {
        if (0 != openTun()) {
            throw new Exception("Could not open Virtual Ethernet Adapter!\n"
                + "Make sure the TAP-Win32 driver is installed.");
        }
    }

    @Override
    public String getDev() {
        return dev;
    }

    private native int openTun();

    @Override
    public native void close();

    @Override
    public native void write(byte[] b, int len);

    @Override
    public native int read(byte[] b);

    @Override
    public void setIP(String ip, String subnetmask) {
        super.setIP(ip, subnetmask);
        try {
            String[] ipCmd = {
                "netsh", "interface", "ip", "set", "address", dev, "static", ip, subnetmask
            };
            String[] metricCmd = {
                "netsh", "interface", "ip", "set", "interface", "interface=" + dev,
                "metric=10"
            };

            String[] renameCmd = {
                "netsh", "interface", "set", "interface", "name=" + dev,
                "newname=Lanemu"
            };

            Process p;

            p = Runtime.getRuntime().exec(ipCmd);
            p.waitFor();
            Logger.getLogger("").log(Level.INFO,
                String.format("Setting the IP address via netsh executed with code %d", p.exitValue()));

            p = Runtime.getRuntime().exec(metricCmd);
            p.waitFor();
            Logger.getLogger("").log(Level.INFO,
                String.format("Setting the metric via netsh executed with code %d", p.exitValue()));

            p = Runtime.getRuntime().exec(renameCmd);
            p.waitFor();
            Logger.getLogger("").log(Level.INFO,
                String.format("Setting the name via netsh executed with code %d (will fail when the name is already set)", p.exitValue()));

        } catch (IOException | InterruptedException e) {
            Logger.getLogger("").log(Level.WARNING, "Could not setup the interface!", e);
        }
    }
}
