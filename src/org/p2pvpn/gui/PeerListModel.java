/*
    Copyright 2008, 2009 Wolfgang Ginolas
    Copyright 2023-2025 Nikolay Borodin <Monsterovich>

    This file is part of Lanemu.

    Lanemu is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Lanemu is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Lanemu.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.p2pvpn.gui;

import java.util.Vector;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import org.p2pvpn.network.ConnectionManager;
import org.p2pvpn.network.PeerID;
import org.p2pvpn.network.Router;
import org.p2pvpn.network.RoutungTableListener;

/**
 * The Model for the peer list used in the MainWindow.
 *
 * @author Wolfgang Ginolas
 */
public class PeerListModel implements RoutungTableListener, ListModel {

    ConnectionManager connectionManager;
    Vector<ListDataListener> listeners;
    Vector<PeerID> list;

    public PeerListModel() {
        connectionManager = null;
        listeners = new Vector<>();
        list = new Vector<>();
    }

    @Override
    public void tableChanged(Router router) {
        PeerID[] peers = router.getPeers();
        list = new Vector<>();

        for (PeerID peer : peers) {
            if (!connectionManager.getLocalAddr().equals(peer)) {
                list.add(peer);
            }
        }

        SwingUtilities.invokeLater(this::notifyListeners);
    }

    public void notifyListeners() {
        for (ListDataListener l : listeners) {
            l.contentsChanged(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, 0, list.size()));
        }
    }

    public void setConnectionManager(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
        if (connectionManager != null) {
            connectionManager.getRouter().addTableListener(this);
            tableChanged(connectionManager.getRouter());
        }
    }

    @Override
    public int getSize() {
        return list.size();
    }

    @Override
    public Object getElementAt(int index) {
        return list.get(index);
    }

    @Override
    public void addListDataListener(ListDataListener l) {
        listeners.add(l);
    }

    @Override
    public void removeListDataListener(ListDataListener l) {
        listeners.remove(l);
    }

}
