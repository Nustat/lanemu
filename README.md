# Lanemu

**[Download](https://gitlab.com/Monsterovich/lanemu/-/releases/0.12)** | **[Issues](https://gitlab.com/Monsterovich/lanemu/-/issues)**

![Logo](resources/images/lanemu.svg){width=128px}

Lanemu is a fork of P2PVPN, originally created by Wolfgang Ginolas (http://www.p2pvpn.org/).

![Screenshot](repo/screenshot.png)

# How to use

## Windows

1. Install the TAP-Win32 Adapter OAS driver by running `add_driver.bat` in the driver directory (preferably as administrator).
2. Run `Lanemu.exe` (requires OpenJDK to be installed, recommended) or `Lanemu-legacy.exe` (older version which requires Oracle Java SE 8, 64 bit).

Alternatively, you can use the newer TAP-Windows Adapter V9 driver and download it from the OpenVPN website, for example: [tap-windows-9.21.2](https://build.openvpn.net/downloads/releases/tap-windows-9.21.2.exe)

After installation, you will need to apply [this](https://gitlab.com/Monsterovich/lanemu/-/raw/main/misc/set_use_tapv9.reg?inline=false) registry file so that the application uses the V9 driver.

**Note:** `ComponentId` of the device, in some cases it may be different, for example be `root/tap0901` instead of `tap0901` depending on how the driver was installed (e.g. through Windows settings). This value can be found in the Windows registry via `regedit.exe`.

**Note for OpenJDK:** If you have OpenJDK already installed, you can make a symbolic link on the NTFS file system.

For example:
```bat
mklink /d "C:\Users\monsterovich\Desktop\Lanemu\build\jdk" "C:\path_to_openjdk"
```

You can use `OpenJDKDownloader.exe` for a quick download.

## Linux

The application needs to be run as root via `sudo java -jar Lanemu.jar` or `lanemu-pkexec`.

If you don't want to run the application as root, you can alternatively give the CAP_NET_ADMIN capability to java interpreter.

```
sudo setcap cap_net_admin=eip /path/to/java
```

Then you can run Lanemu as a normal user without `sudo`.

When you start the application, you will be prompted to configure the tap interface via `ifconfig` which requires root privileges.

**Important:** Make sure the `net-tools` package is installed on your system.

If you want to use native tray in GUI mode, make sure that the `ayatana-appindicator3-0.1 libnotify` libraries are installed. In Debian-based distributions, these dependencies can be installed with the command: `sudo apt install libnotify4 libayatana-appindicator3-1`

## FreeBSD

**[Compiling for FreeBSD](https://gitlab.com/Monsterovich/lanemu/-/wikis/Compiling-for-FreeBSD)**

# Running in the background

```
java -jar Lanemu.jar --headless --access=access.dat --name=Supernode --port=2103 --vpn.ip=10.6.10.10 --vpn.mask=255.255.0.0
```

where `access.dat` is a _private_ invitation.

Run Lanemu with the `--help` parameter for extra documentation.

# Creating a network

To create a new network, you need to specify its name. Other values are: 
- VPN network address (ex. `10.6.0.0`);
- Subnet mask (ex. `255.255.0.0`);
- BitTorrent tracker HTTP(S) addresses;
- DHT search;
- Known hosts to which participants will join by default.

The default number of participants is 2¹⁶, which corresponds to the subnet mask.

Next, you need to send invitations to users. There are two types of "certificates" in the application: a _network_ certificate (analogous to the root certificate) and an _access_ certificate, which is signed by the network certificate.

Clicking "Allow invitation of others" will generate an invitation based on the network private keys, which means that the invited users can invite as many other users as they like and create as many private invitations as they like.

If you disable this option, only a private invitation will be generated that can be given an expiration time.

**Do not edit the invitation file** otherwise the signature verification will fail. The `network.*` parameters are checked on the client-side when the invitation text/file is imported, and the `access.*` parameters are checked on the other user side when peers are connecting to each other.

# Joining a network

To join the network, all you have to do is simply accept the invitation. To do this, you need to click on the accept invitation button and import the invitation file into the application or copy its contents into the input field. If the OK button does not appear enabled, the invitation has not passed the verification check.

If the connection to the peers didn't happen through BitTorrent tracker, you can connect to any of the peers manually by specifying `ip:port` in the information window and retrieve other network members through this peer.

A direct connection between all members of the network may not occur immediately. It's sufficient for at least one network member to have a port forwarded. If both network members are behind NAT, they can only connect through a third member.

# Security

The following cryptographic algorithms are used in the application:

- `RSA-2048` for peer authentication.
- `AES-128-GCM` for encrypting peer-to-peer trafic.
- `SHA-512 with RSA` algorithm is used to sign invitations.
