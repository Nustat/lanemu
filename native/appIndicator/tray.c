/*
    Copyright 2017 Serge Zaitsev
    Copyright 2023-2025 Nikolay Borodin <Monsterovich>

    This file is part of App indicator tray icon library for Lanemu.

    Lanemu is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Lanemu is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Lanemu.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>

#include "tray.h"

struct tray tray = {
    .icon = "",
    .tooltip = "",
    .menu = NULL,
    .size = 0,
    .env = NULL,
    .obj = NULL
};

static void menu_cb(struct tray_menu *item) {
    jclass cls = (*tray.env)->GetObjectClass(tray.env, tray.obj);
    jmethodID mid = (*tray.env)->GetMethodID(tray.env, cls, "clickCallback", "(Ljava/awt/MenuItem;)V");

    (*tray.env)->CallVoidMethod(tray.env, tray.obj, mid, item->menu_item);
}

JFUNC(void, addMenu, jobject menu) {
    (void)this;

    if (tray.menu == NULL) {
        tray.menu = (struct tray_menu*)malloc(sizeof(struct tray_menu));
        tray.size = 1;
    } else {
        tray.size++;
        tray.menu = (struct tray_menu*)realloc(tray.menu, tray.size * sizeof(struct tray_menu));
    }

    if (tray.menu == NULL) {
        printf("add menu realloc failed\n");
        return;
    }

    struct tray_menu *item = &tray.menu[tray.size - 1];
    memset(item, 0, sizeof(struct tray_menu));

    jclass menuCls = (*env)->GetObjectClass(env, menu);
    jmethodID menuGetLabelMethod = (*env)->GetMethodID(env, menuCls, "getLabel", "()Ljava/lang/String;");
    jstring menuLabel = (jstring)(*env)->CallObjectMethod(env, menu, menuGetLabelMethod);
    const char *menuLabelChars = (*env)->GetStringUTFChars(env, menuLabel, NULL);

    item->text = strdup(menuLabelChars);
    item->cb = menu_cb;
    item->menu_item = (*env)->NewGlobalRef(env, menu);

    (*env)->ReleaseStringUTFChars(env, menuLabel, menuLabelChars);
}

JFUNC(void, trayRun, jstring tooltip) {
    jclass jclass = (*env)->GetObjectClass(env, this); 
    jfieldID iconField = (*env)->GetFieldID(env, jclass, "icon", "Ljava/lang/String;"); 
    jstring icon = (jstring)(*env)->GetObjectField(env, this, iconField);
    const char *iconChars = (*env)->GetStringUTFChars(env, icon, NULL);
    tray.icon = iconChars;

    const char *tooltipChars = (*env)->GetStringUTFChars(env, tooltip, NULL);
    tray.tooltip = tooltipChars;

    tray.env = env;
    tray.obj = this;

    if (tray_init(&tray) < 0) {
        printf("failed to create tray\n");
    } else {
        while (!tray_loop(1)) {}
    }

    for (size_t i = 0; i < tray.size; i++) {
        free(tray.menu[i].text);
        (*env)->DeleteGlobalRef(env, tray.menu[i].menu_item);
    }

    free(tray.menu);

    (*env)->ReleaseStringUTFChars(env, icon, iconChars);
    (*env)->ReleaseStringUTFChars(env, tooltip, tooltipChars);
}

JFUNC(void, showMessage, jstring caption, jstring text) {
    const char *captionChars = (*env)->GetStringUTFChars(env, caption, NULL);
    const char *textChars = (*env)->GetStringUTFChars(env, text, NULL);

    jclass jclass = (*env)->GetObjectClass(env, this); 
    jfieldID iconField = (*env)->GetFieldID(env, jclass, "icon", "Ljava/lang/String;"); 
    jstring icon = (jstring)(*env)->GetObjectField(env, this, iconField);
    const char *iconChars = (*env)->GetStringUTFChars(env, icon, NULL);

    struct notification *notification = (struct notification*)malloc(sizeof(struct notification));

    notification->caption = strdup(captionChars);
    notification->text = strdup(textChars);
    notification->icon = strdup(iconChars);

    tray_notification_show(notification);

    (*env)->ReleaseStringUTFChars(env, caption, captionChars);
    (*env)->ReleaseStringUTFChars(env, text, textChars);
    (*env)->ReleaseStringUTFChars(env, icon, iconChars);
}

JFUNC(void, test) {
    (void)env;
    (void)this;
}
