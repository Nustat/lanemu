#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Release-amd64 configuration
CND_PLATFORM_Release-amd64=GNU-Linux
CND_ARTIFACT_DIR_Release-amd64=dist/Release
CND_ARTIFACT_NAME_Release-amd64=libTunTapLinux64.so
CND_ARTIFACT_PATH_Release-amd64=dist/Release/libTunTapLinux64.so
CND_PACKAGE_DIR_Release-amd64=dist/Release-amd64/GNU-Linux/package
CND_PACKAGE_NAME_Release-amd64=libtapLinux.so.tar
CND_PACKAGE_PATH_Release-amd64=dist/Release-amd64/GNU-Linux/package/libtapLinux.so.tar
# Release-i386 configuration
CND_PLATFORM_Release-i386=GNU-Linux
CND_ARTIFACT_DIR_Release-i386=dist/Release
CND_ARTIFACT_NAME_Release-i386=libTunTapLinux.so
CND_ARTIFACT_PATH_Release-i386=dist/Release/libTunTapLinux.so
CND_PACKAGE_DIR_Release-i386=dist/Release-i386/GNU-Linux/package
CND_PACKAGE_NAME_Release-i386=libtapLinux.so.tar
CND_PACKAGE_PATH_Release-i386=dist/Release-i386/GNU-Linux/package/libtapLinux.so.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
